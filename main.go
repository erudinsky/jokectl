/*
Copyright © 2023 NAME HERE <erudinsky@gitlab.com>
*/
package main

import "gitlab.com/erudinsky/jokectl/cmd"

func main() {
	cmd.Execute()
}
