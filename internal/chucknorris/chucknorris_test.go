package chucknorris

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestGetJoke(t *testing.T) {
	resp, err := http.Get("https://api.chucknorris.io/jokes/random")
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	joke := result["value"].(string)

	if joke == "" {
		t.Errorf("Expected non-empty joke, got empty string")
	}
}

func TestJoke(t *testing.T) {
	joke := Joke()
	if joke == "" {
		t.Error("Joke() returned empty string")
	}
}
