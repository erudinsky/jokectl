package chucknorris

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func GetJoke() string {
	// https://api.chucknorris.io/
	resp, err := http.Get("https://api.chucknorris.io/jokes/random")
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)

	joke := result["value"].(string)
	// fmt.Println(joke)
	return joke
}

func Joke() string {
	return "This is a joke"
}
