/*
Copyright © 2023 NAME HERE <erudinsky@gitlab.com>
*/
package cmd

import (
	"fmt"
	"time"

	"github.com/schollz/progressbar/v3"
	"github.com/spf13/cobra"
	"gitlab.com/erudinsky/internal/chucknorris"
)

// chucknorrisCmd represents the chucknorris command
var chucknorrisCmd = &cobra.Command{
	Use:   "chucknorris",
	Short: "A command to get a random joke from Chuck Norris",
	Long: `A command to get a random joke from Chuck Norris. 

For example, to get random joke:

./jokectl chucknorris 

`,
	Run: func(cmd *cobra.Command, args []string) {

		fmt.Println("All right, let me get one for you ... ")
		fmt.Println("")

		bar := progressbar.Default(100)
		for i := 0; i < 100; i++ {
			bar.Add(1)
			time.Sleep(10 * time.Millisecond)
		}
		fmt.Println("")

		fmt.Println(chucknorris.GetJoke())

	},
}

func init() {
	rootCmd.AddCommand(chucknorrisCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// chucknorrisCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// chucknorrisCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
