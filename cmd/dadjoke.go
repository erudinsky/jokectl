/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

// dadjokeCmd represents the dadjoke command
var dadjokeCmd = &cobra.Command{
	Use:   "dadjoke",
	Short: "A command to get a random joke from dad",
	Long: `A command to get a random joke from dad

For example, to get random joke:

./jokectl dadjoke

`,
	Run: func(cmd *cobra.Command, args []string) {
		url := "https://icanhazdadjoke.com/"
		req, _ := http.NewRequest("GET", url, nil)

		req.Header.Add("Accept", "text/plain")

		client := &http.Client{}
		resp, err := client.Do(req)

		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		body, _ := ioutil.ReadAll(resp.Body)

		fmt.Println(string(body))
	},
}

func init() {
	rootCmd.AddCommand(dadjokeCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// dadjokeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// dadjokeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
